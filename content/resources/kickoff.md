---
title: "Kick-off Slides"
description: "A link to the slides used for the seminar kick-off"
weight: 30
# draft: true
---


Here is the [PPT slides](/docs/seminar_kickoff.pdf) used during the seminar kick-off.