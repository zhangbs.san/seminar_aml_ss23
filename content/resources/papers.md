---
title: "Recommended Papers"
description: "A list of great papers on adversarial machine learning research"
weight: 10
# draft: true
---

Here is the [Google spreadsheet](https://docs.google.com/spreadsheets/d/14YlY6msw3kkdxEqIpIRbm4WUIG256JAL/edit?rtpof=true&sd=true) of all the topics and papers (including bonus topics) planned for this course.