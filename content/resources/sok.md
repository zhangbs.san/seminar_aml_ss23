---
title: "SoK Project"
description: "Useful websites for the final SoK project"
weight: 5
# draft: true
---

Journal of Systems Research (JSys) provides a clear summarization of what a systemization of knowledge (SoK) paper looks like. Check [this website](https://www.jsys.org/type_SoK/#:~:text=Systemization%20of%20Knowledge%20(SoK)%20papers%20evaluate%2C%20systematize%2C%20and,an%20established%2C%20major%20research%20area.) for more information.

The IEEE Symposium on Security and Privacy has included papers on Systematization of Knowledge. Feel free to visit [this website](https://oaklandsok.github.io/) for examples of published SoK papers.

<br>