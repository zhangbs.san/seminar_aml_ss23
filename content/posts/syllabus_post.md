---
title: "Syllabus Posted"
description: "A link to the course syllabus (created by Xiao Zhang on 6 Apr, 23)."
# draft: true
weight: -1
---

The [course syllabus](/syllabus/) is now posted. 