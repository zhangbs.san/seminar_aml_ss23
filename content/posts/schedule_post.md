---
title: "Course Schedule & Topic Assignment Posted"
description: "Course schedule and topic assignment are finalized (created by Xiao Zhang on 26 Apr, 23)."
# draft: true
weight: -5
---

The page for [course schedule](/schedule/) and the page for [topic & team assignment](/teams/) are now posted. 