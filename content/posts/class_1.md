---
title: "Class 1: Adversarial Examples & Robustness Evaluation"
description: "A blog post for class 1 (created by Xiao Zhang on 13 May, 23)."
weight: -10
draft: false
---

Here are the [PPT slides](/docs/slides_topic1_shreyash.pdf) of the presentation. The slides were made by Shreyash Arya.

In the first class, Shreyash presented the following two reseach papers related to the topic of the week: 
1. [Towards Deep Learning Models Resistant to Adversarial Attacks](https://arxiv.org/pdf/1706.06083.pdf)
2. [Obfuscated Gradients Give a False Sense of Security: Circumventing Defenses to Adversarial Examples](https://arxiv.org/pdf/1802.00420.pdf)

After the presentation, Shreyash led an engaging discussion, where we mainly discussed some technical details about adversarial training and obfuscated gradients, the connection between the two papers and why the robustness evaluation is important for adversarial machine learning research. 

