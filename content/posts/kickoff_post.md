---
title: "Kick-off Slides Posted"
description: "A link to the slides for seminar kick-off (created by Xiao Zhang on 14 Apr, 23)."
# draft: true
weight: -3
---

The [PPT slides](/docs/seminar_kickoff.pdf) used during the seminar kick-off on Apr. 13 are now posted.